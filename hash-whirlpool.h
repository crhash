#ifndef _CRHASH_WHIRLPOOL_H
#define _CRHASH_WHIRLPOOL_H
void *whirlpool_init_context(void);
void whirlpool_fini_context(void *_ctx);
void whirlpool_update(void *_ctx, const uint8_t *m);
void _whirlpool_update(void *_ctx, const uint8_t *m, unsigned int len);
void whirlpool_fini(void *_ctx);
void whirlpool_digest(void *_ctx, uint8_t *digest);
#endif
