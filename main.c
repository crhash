#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "hash-md5.h"
#include "hash-sha1.h"
#include "hash-whirlpool.h"

static int rv;

static const char *hash;
struct hash_algo {
	const char *name;
	unsigned int block_size;	/* in bits */
	unsigned int digest_size;	/* in bits */
	void *(*init_context)(void);
	void (*fini_context)(void *ctx);
	void (*update)(void *ctx, const uint8_t *m);
	void (*_update)(void *ctx, const uint8_t *m, unsigned int len);
	void (*fini)(void *ctx);
	void (*digest)(void *ctx, uint8_t *digest);
};

static const struct hash_algo _hash_algo[] = {
	{
		.name		= "md5",
		.block_size	= 512,
		.digest_size	= 128,
		.init_context	= md5_init_context,
		.fini_context	= md5_fini_context,
		.update		= md5_update,
		._update	= _md5_update,
		.fini		= md5_fini,
		.digest		= md5_digest,
	},
	{
		.name		= "sha1",
		.block_size	= 512,
		.digest_size	= 160,
		.init_context	= sha1_init_context,
		.fini_context	= sha1_fini_context,
		.update		= sha1_update,
		._update	= _sha1_update,
		.fini		= sha1_fini,
		.digest		= sha1_digest,
	},
	{
		.name		= "whirlpool",
		.block_size	= 512,
		.digest_size	= 512,
		.init_context	= whirlpool_init_context,
		.fini_context	= whirlpool_fini_context,
		.update		= whirlpool_update,
		._update	= _whirlpool_update,
		.fini		= whirlpool_fini,
		.digest		= whirlpool_digest,
	},
};

static const struct hash_algo *find_hash_algo(const char *name)
{
	int i;

	for (i = 0; i < sizeof(_hash_algo) / sizeof(_hash_algo[0]); i++) {
		if (strcmp(name, _hash_algo[i].name) == 0)
			return &_hash_algo[i];
	}
	return NULL;
}

static void _hash_file(const struct hash_algo *hash_algo, const uint8_t *m, uintmax_t len)
{
	uint8_t *digest;
	void *ctx;

	digest = malloc(hash_algo->digest_size / 8);
	if (!digest) {
		perror("malloc");
		rv = EXIT_FAILURE;
		return;
	}

	ctx = hash_algo->init_context();
	if (!ctx) {
		perror("->init");
		free(digest);
		rv = EXIT_FAILURE;
		return;
	}
	while (len >= hash_algo->block_size / 8) {
		hash_algo->update(ctx, m);
		m += hash_algo->block_size / 8;
		len -= hash_algo->block_size / 8;
	}
	hash_algo->_update(ctx, m, len);
	hash_algo->fini(ctx);
	hash_algo->digest(ctx, digest);
	hash_algo->fini_context(ctx);
	{
		int i;

		for (i = 0; i < hash_algo->digest_size / 8; i++)
			printf("%02x", digest[i]);
		printf("\n");
	}
}

static void hash_file(const struct hash_algo *hash_algo, const char *filename)
{
	int fd;
	struct stat st;
	uintmax_t len;
	uint8_t *p;

	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		perror(filename);
		rv = EXIT_FAILURE;
		return;
	}
	if (fstat(fd, &st) == -1) {
		perror(filename);
		rv = EXIT_FAILURE;
		return;
	}
	if (st.st_size < 0) {
		fprintf(stderr, "st_size %" PRIdMAX "\n", (intmax_t)st.st_size);
		rv = EXIT_FAILURE;
		close(fd);
		return;
	}
	len = (uintmax_t)(intmax_t)st.st_size;
	if (len != (uintmax_t)(size_t)len) {
		fprintf(stderr, "st_size %" PRIuMAX "\n", len);
		rv = EXIT_FAILURE;
		close(fd);
		return;
	}
	if (len == 0) {
		p = NULL;
	} else {
		p = mmap(NULL, len, PROT_READ, MAP_PRIVATE, fd, 0);
		if (p == MAP_FAILED) {
			perror("mmap");
			rv = EXIT_FAILURE;
			close(fd);
			return;
		}
		(void)madvise(p, len, MADV_SEQUENTIAL);
	}
	close(fd);

	_hash_file(hash_algo, p, len);

	if (len != 0)
		munmap(p, len);
}

int main(int argc, char *argv[])
{
	int ch;
	const struct hash_algo *hash_algo;

	while ((ch = getopt(argc, argv, "t:")) != -1) {
		switch (ch) {
		case 't':
			hash = optarg;
			break;
		default:
			return EXIT_FAILURE;
		}
	}

	if (hash) {
		hash_algo = find_hash_algo(hash);
		if (!hash_algo) {
			int i;

			fprintf(stderr, "unexpected hash algorithm '%s'\n", hash);
			fprintf(stderr, "expected hash algorithms:");
			for (i = 0; i < sizeof(_hash_algo) / sizeof(_hash_algo[0]); i++)
				fprintf(stderr, " %s", _hash_algo[i].name);
			fprintf(stderr, "\n");
			exit(EXIT_FAILURE);
		}
	} else {
		fprintf(stderr, "hash algorithm required (-t)\n");
		exit(EXIT_FAILURE);
	}

	rv = EXIT_SUCCESS;
	while (optind < argc) {
		hash_file(hash_algo, argv[optind]);
		optind++;
	}
	return rv;
}
