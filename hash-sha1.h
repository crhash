#ifndef _CRHASH_SHA1_H
#define _CRHASH_SHA1_H
void *sha1_init_context(void);
void sha1_fini_context(void *_ctx);
void sha1_update(void *_ctx, const uint8_t *m);
void _sha1_update(void *_ctx, const uint8_t *m, unsigned int len);
void sha1_fini(void *_ctx);
void sha1_digest(void *_ctx, uint8_t *digest);
#endif
