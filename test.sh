#!/bin/sh
cd t
for T in md5 sha1 whirlpool; do
	for M in $T-*.m; do
		DIGEST1=$(../crhash -t $T $M)
		DIGEST2=$(cat $(basename $M .m).$T)
		if test "$DIGEST1" = "$DIGEST2"; then
			echo "PASS $T $M"
		else
			echo "FAIL $T $M $DIGEST1 $DIGEST2"
		fi
	done
done
