PHONY :=

OBJS :=
OBJS += hash-md5.o
OBJS += hash-sha1.o
OBJS += hash-whirlpool.o
OBJS += main.o

CFLAGS ?= -march=native -O2
CFLAGS += -pipe
CFLAGS += -std=c99 -pedantic-errors
CFLAGS += -fno-strict-aliasing
CFLAGS += -Wall

CFLAGS += -D_BSD_SOURCE

LDFLAGS ?= -Wl,-O1

CFLAGS += -ggdb
LDFLAGS += -ggdb

CFLAGS += -Wp,-MD,$(@D)/.$(@F).d

PHONY += all
all: crhash

crhash: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

-include $(wildcard .*.d)

PHONY += test
test: all
	@./test.sh

PHONY += clean
clean:
	$(RM) crhash $(OBJS)
	@find . -type f -name '.*.d' -exec $(RM) {} \;

.PHONY: $(PHONY)
