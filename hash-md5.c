#include <endian.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "hash-md5.h"

static inline uint32_t rol32(const uint32_t x, const unsigned int n)
{
	return (x << n) | (x >> (32 - n));
}

static inline uint32_t F(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return (X & Y) | (~X & Z);
}

static inline uint32_t G(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return (X & Z) | (Y & ~Z);
}

static inline uint32_t H(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return X ^ Y ^ Z;
}

static inline uint32_t I(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return Y ^ (X | ~Z);
}

struct md5_context {
	uint32_t A, B, C, D;
	uint64_t len;	/* in bits */
	uint8_t m[64];
	unsigned int m_len;
};

#define MD5(a, b, c, d, F, k, s, Ti)	\
	a = b + rol32(a + F(b, c, d) + X[k] + Ti, s)

static void __md5_update(void *_ctx, const uint8_t *m)
{
	struct md5_context *ctx = _ctx;
	uint32_t A, B, C, D;
	uint32_t X[16];
	int i;

	A = ctx->A;
	B = ctx->B;
	C = ctx->C;
	D = ctx->D;
	for (i = 0; i < 16; i++)
		X[i] = le32toh(*(uint32_t *)(m + i * sizeof(uint32_t)));

	MD5(A, B, C, D, F,  0,  7, 0xd76aa478);
	MD5(D, A, B, C, F,  1, 12, 0xe8c7b756);
	MD5(C, D, A, B, F,  2, 17, 0x242070db);
	MD5(B, C, D, A, F,  3, 22, 0xc1bdceee);
	MD5(A, B, C, D, F,  4,  7, 0xf57c0faf);
	MD5(D, A, B, C, F,  5, 12, 0x4787c62a);
	MD5(C, D, A, B, F,  6, 17, 0xa8304613);
	MD5(B, C, D, A, F,  7, 22, 0xfd469501);
	MD5(A, B, C, D, F,  8,  7, 0x698098d8);
	MD5(D, A, B, C, F,  9, 12, 0x8b44f7af);
	MD5(C, D, A, B, F, 10, 17, 0xffff5bb1);
	MD5(B, C, D, A, F, 11, 22, 0x895cd7be);
	MD5(A, B, C, D, F, 12,  7, 0x6b901122);
	MD5(D, A, B, C, F, 13, 12, 0xfd987193);
	MD5(C, D, A, B, F, 14, 17, 0xa679438e);
	MD5(B, C, D, A, F, 15, 22, 0x49b40821);

	MD5(A, B, C, D, G,  1,  5, 0xf61e2562);
	MD5(D, A, B, C, G,  6,  9, 0xc040b340);
	MD5(C, D, A, B, G, 11, 14, 0x265e5a51);
	MD5(B, C, D, A, G,  0, 20, 0xe9b6c7aa);
	MD5(A, B, C, D, G,  5,  5, 0xd62f105d);
	MD5(D, A, B, C, G, 10,  9, 0x02441453);
	MD5(C, D, A, B, G, 15, 14, 0xd8a1e681);
	MD5(B, C, D, A, G,  4, 20, 0xe7d3fbc8);
	MD5(A, B, C, D, G,  9,  5, 0x21e1cde6);
	MD5(D, A, B, C, G, 14,  9, 0xc33707d6);
	MD5(C, D, A, B, G,  3, 14, 0xf4d50d87);
	MD5(B, C, D, A, G,  8, 20, 0x455a14ed);
	MD5(A, B, C, D, G, 13,  5, 0xa9e3e905);
	MD5(D, A, B, C, G,  2,  9, 0xfcefa3f8);
	MD5(C, D, A, B, G,  7, 14, 0x676f02d9);
	MD5(B, C, D, A, G, 12, 20, 0x8d2a4c8a);

	MD5(A, B, C, D, H,  5,  4, 0xfffa3942);
	MD5(D, A, B, C, H,  8, 11, 0x8771f681);
	MD5(C, D, A, B, H, 11, 16, 0x6d9d6122);
	MD5(B, C, D, A, H, 14, 23, 0xfde5380c);
	MD5(A, B, C, D, H,  1,  4, 0xa4beea44);
	MD5(D, A, B, C, H,  4, 11, 0x4bdecfa9);
	MD5(C, D, A, B, H,  7, 16, 0xf6bb4b60);
	MD5(B, C, D, A, H, 10, 23, 0xbebfbc70);
	MD5(A, B, C, D, H, 13,  4, 0x289b7ec6);
	MD5(D, A, B, C, H,  0, 11, 0xeaa127fa);
	MD5(C, D, A, B, H,  3, 16, 0xd4ef3085);
	MD5(B, C, D, A, H,  6, 23, 0x04881d05);
	MD5(A, B, C, D, H,  9,  4, 0xd9d4d039);
	MD5(D, A, B, C, H, 12, 11, 0xe6db99e5);
	MD5(C, D, A, B, H, 15, 16, 0x1fa27cf8);
	MD5(B, C, D, A, H,  2, 23, 0xc4ac5665);

	MD5(A, B, C, D, I,  0,  6, 0xf4292244);
	MD5(D, A, B, C, I,  7, 10, 0x432aff97);
	MD5(C, D, A, B, I, 14, 15, 0xab9423a7);
	MD5(B, C, D, A, I,  5, 21, 0xfc93a039);
	MD5(A, B, C, D, I, 12,  6, 0x655b59c3);
	MD5(D, A, B, C, I,  3, 10, 0x8f0ccc92);
	MD5(C, D, A, B, I, 10, 15, 0xffeff47d);
	MD5(B, C, D, A, I,  1, 21, 0x85845dd1);
	MD5(A, B, C, D, I,  8,  6, 0x6fa87e4f);
	MD5(D, A, B, C, I, 15, 10, 0xfe2ce6e0);
	MD5(C, D, A, B, I,  6, 15, 0xa3014314);
	MD5(B, C, D, A, I, 13, 21, 0x4e0811a1);
	MD5(A, B, C, D, I,  4,  6, 0xf7537e82);
	MD5(D, A, B, C, I, 11, 10, 0xbd3af235);
	MD5(C, D, A, B, I,  2, 15, 0x2ad7d2bb);
	MD5(B, C, D, A, I,  9, 21, 0xeb86d391);

	ctx->A += A;
	ctx->B += B;
	ctx->C += C;
	ctx->D += D;
}

void md5_update(void *_ctx, const uint8_t *m)
{
	struct md5_context *ctx = _ctx;

	__md5_update(ctx, m);
	ctx->len += 512;
}

void _md5_update(void *_ctx, const uint8_t *m, unsigned int len)
{
	struct md5_context *ctx = _ctx;

	while (len > 0) {
		ctx->m[ctx->m_len] = *m;
		ctx->m_len++;
		m++;
		len--;
		ctx->len += 8;

		if (ctx->m_len == 64) {
			__md5_update(ctx, ctx->m);
			ctx->m_len = 0;
		}
	}
}

void *md5_init_context(void)
{
	struct md5_context *ctx;

	ctx = malloc(sizeof(struct md5_context));
	if (!ctx)
		return NULL;

	ctx->A = 0x67452301;
	ctx->B = 0xefcdab89;
	ctx->C = 0x98badcfe;
	ctx->D = 0x10325476;
	ctx->len = 0;

	memset(ctx->m, 0, 64);
	ctx->m_len = 0;

	return ctx;
}

void md5_fini_context(void *_ctx)
{
	struct md5_context *ctx = _ctx;

	memset(ctx, 0, sizeof(struct md5_context));
	free(ctx);
}

void md5_fini(void *_ctx)
{
	struct md5_context *ctx = _ctx;

	ctx->m[ctx->m_len] = 0x80;
	ctx->m_len++;
	if (ctx->m_len <= 64 - 8) {
		while (ctx->m_len <= 64 - 8) {
			ctx->m[ctx->m_len] = 0;
			ctx->m_len++;
		}
	} else {
		while (ctx->m_len < 64) {
			ctx->m[ctx->m_len] = 0;
			ctx->m_len++;
		}
		__md5_update(ctx, ctx->m);
		memset(ctx->m, 0, 64 - 8);
	}
	*(uint64_t *)&ctx->m[64 - 8] = htole64(ctx->len);
	__md5_update(ctx, ctx->m);
}

void md5_digest(void *_ctx, uint8_t *digest)
{
	struct md5_context *ctx = _ctx;

	*(uint32_t *)&digest[0] = htole32(ctx->A);
	*(uint32_t *)&digest[4] = htole32(ctx->B);
	*(uint32_t *)&digest[8] = htole32(ctx->C);
	*(uint32_t *)&digest[12] = htole32(ctx->D);
}
