#ifndef _CRHASH_MD5_H
#define _CRHASH_MD5_H
void *md5_init_context(void);
void md5_fini_context(void *_ctx);
void md5_update(void *_ctx, const uint8_t *m);
void _md5_update(void *_ctx, const uint8_t *m, unsigned int len);
void md5_fini(void *_ctx);
void md5_digest(void *_ctx, uint8_t *digest);
#endif
