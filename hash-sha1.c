#include <endian.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "hash-sha1.h"

static inline uint32_t rol32(const uint32_t x, const unsigned int n)
{
	return (x << n) | (x >> (32 - n));
}

static inline uint32_t F(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return (X & Y) | (~X & Z);
}

static inline uint32_t G(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return X ^ Y ^ Z;
}

static inline uint32_t H(const uint32_t X, const uint32_t Y, const uint32_t Z)
{
	return (X & Y) ^ (X & Z) ^ (Y & Z);
}

struct sha1_context {
	uint32_t A, B, C, D, E;
	uint64_t len;	/* in bits */
	uint8_t m[64];
	unsigned int m_len;
};

static void __sha1_update(void *_ctx, const uint8_t *m)
{
	struct sha1_context *ctx = _ctx;
	uint32_t A, B, C, D, E;
	uint32_t W[80];
	int i;

	A = ctx->A;
	B = ctx->B;
	C = ctx->C;
	D = ctx->D;
	E = ctx->E;

	for (i = 0; i < 16; i++)
		W[i] = be32toh(*(uint32_t *)(m + i * sizeof(uint32_t)));
	for (i = 16; i < 80; i++)
		W[i] = rol32(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);

	for (i = 0; i <= 19; i++) {
		uint32_t T;

		T = rol32(A, 5) + F(B, C, D) + E + 0x5a827999 + W[i];
		E = D;
		D = C;
		C = rol32(B, 30);
		B = A;
		A = T;
	}
	for (i = 20; i <= 39; i++) {
		uint32_t T;

		T = rol32(A, 5) + G(B, C, D) + E + 0x6ed9eba1 + W[i];
		E = D;
		D = C;
		C = rol32(B, 30);
		B = A;
		A = T;
	}
	for (i = 40; i <= 59; i++) {
		uint32_t T;

		T = rol32(A, 5) + H(B, C, D) + E + 0x8f1bbcdc + W[i];
		E = D;
		D = C;
		C = rol32(B, 30);
		B = A;
		A = T;
	}
	for (i = 60; i <= 79; i++) {
		uint32_t T;

		T = rol32(A, 5) + G(B, C, D) + E + 0xca62c1d6 + W[i];
		E = D;
		D = C;
		C = rol32(B, 30);
		B = A;
		A = T;
	}

	ctx->A += A;
	ctx->B += B;
	ctx->C += C;
	ctx->D += D;
	ctx->E += E;
}

void sha1_update(void *_ctx, const uint8_t *m)
{
	struct sha1_context *ctx = _ctx;

	__sha1_update(ctx, m);
	ctx->len += 512;
}

void _sha1_update(void *_ctx, const uint8_t *m, unsigned int len)
{
	struct sha1_context *ctx = _ctx;

	while (len > 0) {
		ctx->m[ctx->m_len] = *m;
		ctx->m_len++;
		m++;
		len--;
		ctx->len += 8;

		if (ctx->m_len == 64) {
			__sha1_update(ctx, ctx->m);
			ctx->m_len = 0;
		}
	}
}

void *sha1_init_context(void)
{
	struct sha1_context *ctx;

	ctx = malloc(sizeof(struct sha1_context));
	if (!ctx)
		return NULL;

	ctx->A = 0x67452301;
	ctx->B = 0xefcdab89;
	ctx->C = 0x98badcfe;
	ctx->D = 0x10325476;
	ctx->E = 0xc3d2e1f0;
	ctx->len = 0;

	memset(ctx->m, 0, 64);
	ctx->m_len = 0;

	return ctx;
}

void sha1_fini_context(void *_ctx)
{
	struct sha1_context *ctx = _ctx;

	memset(ctx, 0, sizeof(struct sha1_context));
	free(ctx);
}

void sha1_fini(void *_ctx)
{
	struct sha1_context *ctx = _ctx;

	ctx->m[ctx->m_len] = 0x80;
	ctx->m_len++;
	if (ctx->m_len <= 64 - 8) {
		while (ctx->m_len <= 64 - 8) {
			ctx->m[ctx->m_len] = 0;
			ctx->m_len++;
		}
	} else {
		while (ctx->m_len < 64) {
			ctx->m[ctx->m_len] = 0;
			ctx->m_len++;
		}
		__sha1_update(ctx, ctx->m);
		memset(ctx->m, 0, 64 - 8);
	}
	*(uint64_t *)&ctx->m[64 - 8] = htobe64(ctx->len);
	__sha1_update(ctx, ctx->m);
}

void sha1_digest(void *_ctx, uint8_t *digest)
{
	struct sha1_context *ctx = _ctx;

	*(uint32_t *)&digest[0] = htobe32(ctx->A);
	*(uint32_t *)&digest[4] = htobe32(ctx->B);
	*(uint32_t *)&digest[8] = htobe32(ctx->C);
	*(uint32_t *)&digest[12] = htobe32(ctx->D);
	*(uint32_t *)&digest[16] = htobe32(ctx->E);
}
